# wot-helloworld

Simple Hello World mod for World of Tanks. Displays a notification on garage load, prebattle, battle start and when NUMPAD_0 is pressed

![](example_garage.png)

![](example_prebattle.png)

![](example_battle.png)

![](example_chat.png)
