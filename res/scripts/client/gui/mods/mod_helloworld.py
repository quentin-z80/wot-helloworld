from gui.app_loader.settings import APP_NAME_SPACE
from gui.shared import g_eventBus, events
from gui import SystemMessages
from gui.shared.personality import ServicesLocator
from gui.Scaleform.framework import WindowLayer
from Avatar import PlayerAvatar
from constants import ARENA_PERIOD
from messenger import MessengerEntry
import Keys
from gui import InputHandler

print "HELLOWORLD MOD LOADED"

class HelloWorld(object):

    def __init__(self):
        self.battlerun = False
        g_eventBus.addListener(events.GUICommonEvent.LOBBY_VIEW_LOADED, self.welcome_garage)

        g_eventBus.addListener(events.AppLifeCycleEvent.INITIALIZING, self.battleLoading)
        g_eventBus.addListener(events.AppLifeCycleEvent.DESTROYED, self.destroyBattle)

    def welcome_garage(self, event):
        print "PUSHING HELLOWORLD MSG"
        SystemMessages.pushMessage("Hello World!", SystemMessages.SM_TYPE.GameGreeting)
        g_eventBus.removeListener(event, self.welcome_garage)

    def battleLoading(self, event):
        if event.ns == APP_NAME_SPACE.SF_BATTLE:
            InputHandler.g_instance.onKeyDown += self.key_event

    def destroyBattle(self, event):
        if event.ns == APP_NAME_SPACE.SF_BATTLE:
            InputHandler.g_instance.onKeyDown -= self.key_event

    def key_event(self, event):
        if event.key == Keys.KEY_NUMPAD0 and event.isKeyDown():
            print "SENDING KEY MSG"
            message = '<font color="#D900D5">KEYPRESS: Hello World!</font>'
            MessengerEntry.g_instance.gui.addClientMessage(message)

hello = HelloWorld()


def new_onArenaPeriodChange(self, period, periodEndTime, periodLength, periodAdditionalInfo):
    old_onArenaPeriodChange(self, period, periodEndTime, periodLength, periodAdditionalInfo)
    if period == ARENA_PERIOD.PREBATTLE:
        print "SENDING PREBATTLE PANEL MSG"
        battle_page = ServicesLocator.appLoader.getDefBattleApp().containerManager.getContainer(WindowLayer.VIEW).getView()
        getattr(battle_page.components['battlePlayerMessages'], 'as_showPurpleMessageS', None)(None, "PREBATTLE: Hello World!")
    elif period == ARENA_PERIOD.BATTLE:
        print "SENDING BATTLE PANEL MSG"
        battle_page = ServicesLocator.appLoader.getDefBattleApp().containerManager.getContainer(WindowLayer.VIEW).getView()
        getattr(battle_page.components['battlePlayerMessages'], 'as_showPurpleMessageS', None)(None, "BATTLE: Hello World!")

old_onArenaPeriodChange = PlayerAvatar._PlayerAvatar__onArenaPeriodChange
PlayerAvatar._PlayerAvatar__onArenaPeriodChange = new_onArenaPeriodChange
